/*

Template: Soft-x - Landing and Startup Agency Template
Author: potenzaglobalsolutions
Design and Developed by: potenzaglobalsolutions.com

NOTE: This file contains all scripts for the actual Template.

*/

/*================================================
[  Table of contents  ]
================================================

:: Menu
:: Counter
:: Owl carousel
:: Magnific Popup
:: typer
:: Countdown
:: Scrollbar
:: Back to top
:: Sticky
:: Tooltip

======================================
[ End table content ]
======================================*/

//POTENZA var
(function ($) {
  "use strict";
  var POTENZA = {};

/*************************
  Predefined Variables
*************************/
  var $window = $(window),
    $document = $(document),
    $body = $('body'),
    $countdownTimer = $('.countdown'),
    $progressBar = $('.skill-bar'),
    $counter = $('.counter');
  //Check if function exists
  $.fn.exists = function () {
    return this.length > 0;
  };

/*************************
      Menu
  *************************/
  POTENZA.dropdownmenu = function () {
    if ($('.navbar').exists()) {
      $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
          $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');
        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
          $('.dropdown-submenu .show').removeClass("show");
        });
        return false;
      });
    }
  };


/*************************
       counter
*************************/
  POTENZA.counters = function () {
    var counter = jQuery(".counter");
    if (counter.length > 0) {
      $counter.each(function () {
        var $elem = $(this);
        $elem.appear(function () {
          $elem.find('.timer').countTo();
        });
      });
    }
  };

  /*************************
       Owl carousel
*************************/
  POTENZA.carousel = function () {
    var owlslider = jQuery("div.owl-carousel");
    if (owlslider.length > 0) {
      owlslider.each(function () {
        var $this = $(this),
          $items = ($this.data('items')) ? $this.data('items') : 1,
          $loop = ($this.attr('data-loop')) ? $this.data('loop') : true,
          $navdots = ($this.data('nav-dots')) ? $this.data('nav-dots') : false,
          $navarrow = ($this.data('nav-arrow')) ? $this.data('nav-arrow') : true,
          $autoplay = ($this.attr('data-autoplay')) ? $this.data('autoplay') : true,
          $autospeed = ($this.attr('data-autospeed')) ? $this.data('autospeed') : 5000,
          $smartspeed = ($this.attr('data-smartspeed')) ? $this.data('smartspeed') : 1000,
          $autohgt = ($this.data('autoheight')) ? $this.data('autoheight') : false,
          $space = ($this.attr('data-space')) ? $this.data('space') : 30,
          $animateOut = ($this.attr('data-animateOut')) ? $this.data('animateOut') : false;

        $(this).owlCarousel({
          loop: $loop,
          items: $items,
          responsive: {
            0: {
              items: $this.data('xx-items') ? $this.data('xx-items') : 1
            },
            480: {
              items: $this.data('xs-items') ? $this.data('xs-items') : 1
            },
            768: {
              items: $this.data('sm-items') ? $this.data('sm-items') : 2
            },
            980: {
              items: $this.data('md-items') ? $this.data('md-items') : 3
            },
            1200: {
              items: $items
            }
          },
          dots: $navdots,
          autoplayTimeout: $autospeed,
          smartSpeed: $smartspeed,
          autoHeight: $autohgt,
          margin: $space,
          nav: $navarrow,
          navText : ["<div class='nav-btn prev-slide testimonials-arrow d-sm-block d-none'><i class='fa fa-chevron-left'></i></div>","<div class='nav-btn next-slide testimonials-arrow d-sm-block d-none'><i class='fa fa-chevron-right'></i></div>"],
          autoplay: $autoplay,
          autoplayHoverPause: true
        });
      });
    }
  }

  /*************************
      Magnific Popup
  *************************/
  POTENZA.mediaPopups = function () {
    if ($(".popup-single").exists() || $(".popup-gallery").exists() || $('.modal-onload').exists() || $(".popup-youtube, .popup-vimeo, .popup-gmaps").exists()) {
      if ($(".popup-single").exists()) {
        $('.popup-single').magnificPopup({
          type: 'image'
        });
      }
      if ($(".popup-gallery").exists()) {
        $('.popup-gallery').magnificPopup({
          delegate: 'a.portfolio-img',
          type: 'image',
          tLoading: 'Loading image #%curr%...',
          mainClass: 'mfp-img-mobile',
          gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
          }
        });
      }
      if ($(".popup-youtube, .popup-vimeo, .popup-gmaps").exists()) {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });
      }
      var $modal = $('.modal-onload');
      if ($modal.length > 0) {
        $('.popup-modal').magnificPopup({
          type: 'inline'
        });
        $(document).on('click', '.popup-modal-dismiss', function (e) {
          e.preventDefault();
          $.magnificPopup.close();
        });
        var elementTarget = $modal.attr('data-target');
        setTimeout(function () {
          $.magnificPopup.open({
            items: {
              src: elementTarget
            },
            type: "inline",
            mainClass: "mfp-no-margins mfp-fade",
            closeBtnInside: !0,
            fixedContentPos: !0,
            removalDelay: 500
          }, 0)
        }, 1500);
      }
    }
  }


  /*************************
           Countdown
  *************************/
  POTENZA.countdownTimer = function () {
    if ($countdownTimer.exists()) {
      $countdownTimer.downCount({
        date: '12/25/2020 12:00:00', // Month/Date/Year HH:MM:SS
        offset: -4
      });
    }
  }


  /*************************
      scrollbar
  *************************/
  POTENZA.scrollbar = function () {
    var scrollbar = jQuery(".scrollbar");
    if (scrollbar.length > 0) {
      //Sidebar Scroll
      var scroll_light = jQuery(".scroll_light");
      if (scroll_light.length > 0) {
        $(scroll_light).niceScroll({
          cursorborder: 0,
          cursorcolor: "rgba(255,255,255,0.25)"
        });
        $(scroll_light).getNiceScroll().resize();
      }
      // Chat Scroll
      var scroll_dark = jQuery(".scroll_dark");
      if (scroll_dark.length > 0) {
        $(scroll_dark).niceScroll({
          cursorborder: 0,
          cursorcolor: "rgba(0,0,0,0.1)"
        });
        $(scroll_dark).getNiceScroll().resize();
      }
    }
  }


/*************************
       Progressbar
*************************/
    POTENZA.progressBar = function () {
        if ($progressBar.exists()) {
            $progressBar.each(function (i, elem) {
                var $elem = $(this),
                    percent = $elem.attr('data-percent') || "100",
                    delay = $elem.attr('data-delay') || "100",
                    type = $elem.attr('data-type') || "%";

                if (!$elem.hasClass('progress-animated')) {
                    $elem.css({
                        'width': '0%'
                    });
                }
                var progressBarRun = function () {
                    $elem.animate({
                        'width': percent + '%'
                    }, 'easeInOutCirc').addClass('progress-animated');

                    $elem.delay(delay).append('<span class="progress-type animated fadeIn">' + type + '</span><span class="progress-number animated fadeIn">' + percent + '</span>');
                };

                    $(elem).appear(function () {
                        setTimeout(function () {
                            progressBarRun();
                        }, delay);
                    });
            });
        }
    };

/*************************
         sticky
*************************/

POTENZA.isSticky = function () {
  $(window).scroll(function(){
    if ($(this).scrollTop() > 150) {
       $('.header-sticky').addClass('is-sticky');
    } else {
       $('.header-sticky').removeClass('is-sticky');
    }
  });
};

/*************************
         Isotope
*************************/
POTENZA.Isotope = function () {
      var $isotope = $(".isotope"),
          $itemElement = '.grid-item',
          $filters = $('.isotope-filters');
        if ($isotope.exists()) {
            $isotope.isotope({
            resizable: true,
            itemSelector: $itemElement,
              masonry: {
                gutterWidth: 10
              }
            });
             $filters.on( 'click', 'button', function() {
               var $val = $(this).attr('data-filter');
                   $isotope.isotope({ filter: $val });
                   $filters.find('.active').removeClass('active');
                   $(this).addClass('active');
            });
        }
}

/*************************
        tooltip
*************************/
POTENZA.tooltip = function () {
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip()
  })
}

/*************************
       search-toggle
*************************/
POTENZA.searchtoggle = function () {
  $(document).ready(function(){
        $(".search-toggle").click(function(){
          $(".search-box").toggleClass("open");
        });
      });
}


/*************************
       search-toggle
*************************/
POTENZA.svginjector = function () {
  $(document).ready(function(){
          // Elements to inject
    var mySVGsToInject = document.querySelectorAll('img.svg-injector');

    // Do the injection
    SVGInjector(mySVGsToInject);

      });
}

  /****************************************************
       POTENZA Window load and functions
  ****************************************************/
  //Window load functions
  $window.on("load", function () {
    POTENZA.Isotope();
  });
  //Document ready functions
  $document.ready(function () {
    POTENZA.counters(),
    POTENZA.dropdownmenu(),
    POTENZA.scrollbar(),
    POTENZA.countdownTimer(),
    POTENZA.mediaPopups(),
    POTENZA.carousel(),
    POTENZA.isSticky(),
    POTENZA.Isotope(),
    POTENZA.progressBar(),
    POTENZA.tooltip(),
    POTENZA.searchtoggle(),
    POTENZA.svginjector()
  });

  // Anchor Menu
  const links = $('.link-anchor');
 
  links.on("click", function clickHandler(event) {
    event.preventDefault();
    const href = this.getAttribute("href");
    const offsetTop = document.querySelector(href).offsetTop;

    scroll({
      top: offsetTop,
      behavior: "smooth"
    });
  });

  //Ao clicar no botão de teste grátis
  $('#free-test').on('click', () => {
    $('#form-email0').focus();
  })

  const createEventRegisterUser = ({
    button,
    messageError,
    inputEmail,
  }) => {
      button.on('click', event => {
        event.preventDefault(event)
        customerCreate(button, messageError, inputEmail)
    })
      return button;
  }
   
   ['#teste-gratis0', '#teste-gratis1', '#teste-gratis2'].map((item, index) => {
    createEventRegisterUser ({
       button: $(item),
       messageError: $(`#message-error${index}`),
       inputEmail: $(`#form-email${index}`)
     })     
   }) 
  
  function customerCreate(button, messageError, email){  
    let customerEmail = email.val().trim().toLowerCase()
    let userEmail = customerEmail.substr(0, customerEmail.indexOf("@"))
    let providerEmail = customerEmail.substr(customerEmail.indexOf("@")+ 1, customerEmail.length)
    
    const isStaging = !!window.location.host.match(/staging/)
    let endpointURL = 'https://api-v2.pandavideo.com.br/user/create'
    let redirectURL = `https://dashboard.pandavideo.com.br/?email=${customerEmail}&type=first-access`
    if (isStaging) {
      endpointURL = 'https://api-v2-staging.pandavideo.com.br/user/create'
      redirectURL = `https://dashboard.pandavideo.com.br/?email=${customerEmail}&type=first-access`
    }
    
    if (
        (userEmail.length >=1) &&
        (userEmail.search(" ")==-1) &&
        (providerEmail.length >=3) &&
        (providerEmail.search("@")==-1) &&
        (providerEmail.search(" ")==-1) &&
        (providerEmail.search(".")!=-1) &&
        (providerEmail.indexOf(".") >=1)&&
        (providerEmail.lastIndexOf(".") < providerEmail.length - 1)) {
          const wrongsEmail = ['gmail.co', 'gmal.com', 'gmai.com', 'hotmal.com', 'hotmail.co', 'hotmai.com'];
          if (wrongsEmail.indexOf(providerEmail) != -1) {
            const check = confirm("Acho que seu e-mail " + customerEmail + " está errado. Tem certeza?");
            if (check) {  
              handleRegister()  
            }
            else{
                email.focus()
            }
          }

          else{
            handleRegister()
          }
      }

      else{        
        messageError.html(`<p>Por favor, digite um e-mail válido!</p>`)
      }

    email.on('input', () => messageError.html(``))

    function handleRegister(){    
      const customerEmail = email.val().trim().toLowerCase()
      const paramsUrl = new URLSearchParams(window.location.search)
      button.toggleClass('disabled') 
      button.html('Criando conta… <i class="fa fa-spinner fa-spin-load" aria-hidden="true"></i>')
      let objRegister = {
          email: customerEmail,
          name: customerEmail
      }
      if (paramsUrl.get('ref')) {
        objRegister['ref_tap_filiate'] = paramsUrl.get('ref')
        objRegister['ref_tap_filiate_c'] = paramsUrl.get('refc') ? paramsUrl.get('refc') : 'br'
        redirectURL = redirectURL + "&ref=" + paramsUrl.get('ref') + "&refc=" +  paramsUrl.get('refc')
      }
      axios({
        method : "POST",
        data: objRegister,
        headers: {
          "Content-Type": "application/json"
        },
        baseURL : endpointURL
      })
      .then(response => { 
        window.location.replace(redirectURL + "#/")
        button.html('Teste grátis por 7 dias')
        button.toggleClass('disabled') 
      })
        .catch(error => {
          if(error.response.status == 400){
            console.error(error)
            button.html('Teste grátis por 7 dias')
            button.toggleClass('disabled') 
            messageError.html(`<p>E-mail já cadastrado. <a href="${redirectURL}" target="_blank">Acesse agora &raquo</a></p>`)
          }
          else{
            console.error(error)
            button.html('Teste grátis por 7 dias') 
            button.toggleClass('disabled') 
            messageError.html(`<p>Ops! Houve uma falha no nosso sistema. <a onclick="${handleRegister(event)}">Tentar novamente »</a></p>`)
          }
        });
    }
  }  

  const owl = $('.owl-carousel-prices');
  owl.owlCarousel({
      loop:false,
      URLhashListener:true,
      startPosition: 'URLHash',
    //margin:10,
      autoplay:false,
      nav: true,
      dots:true,
      navText : ["<div class='nav-btn prev-slide' style='left: -25px;'><i class='fa fa-chevron-left' style='padding: 16px 7px 0px 5px;'></i></div>","<div class='nav-btn next-slide' style='right: -20px;'><i class='fa fa-chevron-right'></i></div>"],
      responsive:{
        0:{
            items:1,
            nav:true,
            autoWidth:true,
        },
        770:{
            items:3,
            nav:true,
            autoWidth:false,
        },
        1000:{
            items:4,
            nav:true,
            autoWidth:false,
        }
    }
      //autoplayTimeout:1000,
    //autoplayHoverPause:true
  });
  /* Ocasional Autoplay 
  $('.play').on('click',function(){
      owl.trigger('play.owl.autoplay',[1000])
  })
  $('.stop').on('click',function(){
      owl.trigger('stop.owl.autoplay')
  })
  */


  let timeVideos = 10
  let numberVideos = 50
  let numberStudents = 100
  
  const createEventRangeOption = ({
    elem,
    outputInfo,
    outputInfoMax,
    outputInfoValue
  }) => {
      elem.on('input', () => {
        outputInfoValue = parseInt(elem.val())
        outputInfoValue == outputInfoMax ? outputInfo.html(`+ de ${outputInfoValue}`) : outputInfo.html(outputInfoValue)
        outputInfoMax == 120 ? timeVideos = outputInfoValue : timeVideos 
        outputInfoMax == 1000 ? numberVideos = outputInfoValue : numberVideos 
        outputInfoMax == 10000 ? numberStudents = outputInfoValue : numberStudents 
        handlePriceRecommendation(timeVideos, numberVideos, numberStudents)
    })
      return elem;
  }
   
   ['#range-slider-0', '#range-slider-1', '#range-slider-2'].map((item, index) => {
    createEventRangeOption ({
       elem: $(item),
       outputInfo: $(`#output-info${index}`),
       outputInfoMax: $(`#range-slider-${index}`).attr("max"),
     })     
   })      

  function handlePriceRecommendation(timeVideos, numberVideos, numberStudents){
    const storageRecommendation = numberVideos*timeVideos*85
    const trafficRecommendation = (storageRecommendation*0.05)*(numberStudents/3)
    const cardBodyPrice = $('.card-body-price') 
    let numberCard = 1
    numberCard = (storageRecommendation > 50000 && storageRecommendation < 400000 || trafficRecommendation > 5000 && trafficRecommendation < 400000) ? cardRecomended(2) : 1 
    numberCard = (storageRecommendation > 400001 && storageRecommendation < 1000000 || trafficRecommendation > 400001 && trafficRecommendation < 1000000) ? cardRecomended(3) : 1 
    numberCard = (storageRecommendation > 1000001 && storageRecommendation < 5000000 || trafficRecommendation > 1000001 && trafficRecommendation < 5000000) ? cardRecomended(4) : 1 
    numberCard = (storageRecommendation > 1000001 && storageRecommendation < 5000000 || trafficRecommendation > 5000001 && trafficRecommendation < 10000000) ? cardRecomended(5) : 1 
    numberCard = (storageRecommendation > 5000001 && storageRecommendation < 10000000 || trafficRecommendation > 10000001 && trafficRecommendation < 15000000) ? cardRecomended(6) : 1 
    numberCard = (storageRecommendation > 5000001 && storageRecommendation < 10000000 || trafficRecommendation > 15000001 && trafficRecommendation < 30000000) ? cardRecomended(7) : 1 
    numberCard = (storageRecommendation > 10000001 || trafficRecommendation > 30000000) ? cardRecomended(8) : 1  

    function cardRecomended (numberCard){
      const cardRecomended = $(`.card-${numberCard}`)
      cardBodyPrice.removeClass('card-recommended')
      cardRecomended.addClass('card-recommended')  
      window.location.hash = `#card${numberCard}`
    }
  }

})(jQuery);
