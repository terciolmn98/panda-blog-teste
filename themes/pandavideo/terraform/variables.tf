variable "gitlab_token" {
    type = string
}

variable "domain_staging" {
	type    = list(string)
	default = ["staging.pandavideo.com.br", "staging.pandavideos.com.br"]
}

variable "domain_production" {
	type    = list(string)
	default = ["www.pandavideo.com.br", "pandavideo.com.br", "www.pandavideos.com.br", "pandavideos.com.br"]
}