terraform {
    backend "s3" {
        bucket  = "tfstates-panda-video-iac"
        key     = "projects/panda-ms-homepage/terraform.tfstate"
        region  = "us-east-1"
        encrypt = true
    }
    required_providers {
        aws = {
            version = "~> 3.0"
            source  = "hashicorp/aws"
        }
        gitlab = {
            source = "gitlabhq/gitlab"
        }
    }
}