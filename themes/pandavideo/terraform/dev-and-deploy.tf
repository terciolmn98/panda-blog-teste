provider "gitlab" {
    token = var.gitlab_token   
}

resource "gitlab_project" "homepage" {
    name = "Homepage"
    path = "homepage"
    description = "www.pandavideo.com.br"
    default_branch = "master"
    issues_enabled = true
    merge_requests_enabled = true
    remove_source_branch_after_merge = true
    wiki_enabled = false
    snippets_enabled = false
    container_registry_enabled = true
    visibility_level = "private"
    lfs_enabled = false
    only_allow_merge_if_pipeline_succeeds = true
    request_access_enabled = false
}

resource "gitlab_branch_protection" "branch_protect_master" {
    project = gitlab_project.homepage.id
    branch = "master"
    push_access_level = "no one"
    merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "branch_protect_staging" {
    project = gitlab_project.homepage.id
    branch = "staging"
    push_access_level = "maintainer"
    merge_access_level = "maintainer"
}

resource "gitlab_project_variable" "secret_id" {
    project = gitlab_project.homepage.id
    key = "AWS_SECRET_ACCESS_KEY"
    value = aws_iam_access_key.homepage_iam_user.secret
    protected = true
    masked = true
    environment_scope = "*"
}

resource "gitlab_project_variable" "key_id" {
    project = gitlab_project.homepage.id
    key = "AWS_ACCESS_KEY_ID"
    value = aws_iam_access_key.homepage_iam_user.id
    protected = true
    masked = true
    environment_scope = "*"
}

resource "aws_iam_user" "deploy_homepage_user" {
    name = "deploy-homepage-user"
    path = "/"

    tags = {
        gitlab = gitlab_project.homepage.web_url
    }
}

resource "aws_iam_access_key" "homepage_iam_user" {
    user = aws_iam_user.deploy_homepage_user.name
}

resource "aws_iam_user_policy" "deploy_s3_dist" {
    name = "Deploy-S3-Dist"
    user = aws_iam_user.deploy_homepage_user.name

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                "${aws_s3_bucket.bucket_staging[0].arn}",
                "${aws_s3_bucket.bucket_staging[0].arn}/*",
                "${aws_s3_bucket.bucket_production[1].arn}",
                "${aws_s3_bucket.bucket_production[1].arn}/*"
            ]
        }
    ]
}
EOF
}