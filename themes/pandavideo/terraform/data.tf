data "aws_route53_zone" "selected" {
  name = "pandavideo.com.br"
}

data "aws_route53_zone" "selected_2" {
  name = "pandavideos.com.br"
}

data "aws_acm_certificate" "issued" {
  domain   = "pandavideo.com.br"
  statuses = ["ISSUED"]
  most_recent = true
}