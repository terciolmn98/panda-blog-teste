
locals {
    number_of_domains_production = length(var.domain_production)
}

resource "aws_s3_bucket" "bucket_production" {
    count = local.number_of_domains_production
    bucket = var.domain_production[count.index]
    acl    = "public-read"

    tags = {
        gitlab = gitlab_project.homepage.web_url
    }

    website {
        index_document = "index.html"
        error_document = count.index == 1 ? "404.html" : "index.html" 
    }
}

resource "aws_s3_bucket_policy" "bucket_policy_production" {
    count = local.number_of_domains_production
    bucket = aws_s3_bucket.bucket_production.*.bucket[count.index]

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.bucket_production.*.bucket[count.index]}/*"
        }
    ]
}
EOF
}

resource "aws_cloudfront_distribution" "s3_distribution_production" {
    count = local.number_of_domains_production
    origin {
        domain_name = aws_s3_bucket.bucket_production.*.website_endpoint[count.index]
        origin_id   = "S3-homepage-${aws_s3_bucket.bucket_production.*.bucket[count.index]}"

        custom_origin_config {
            http_port                = 80
            https_port               = 443
            origin_keepalive_timeout = 5
            origin_protocol_policy   = "http-only"
            origin_read_timeout      = 30
            origin_ssl_protocols     = [
                "TLSv1",
                "TLSv1.1",
                "TLSv1.2"
            ]
        }
    }


    enabled             = true
    is_ipv6_enabled     = true
    default_root_object = "index.html"

    aliases = [aws_s3_bucket.bucket_production.*.bucket[count.index]]

    default_cache_behavior {
        allowed_methods  = ["GET", "HEAD"]
        cached_methods   = ["GET", "HEAD"]
        target_origin_id = "S3-homepage-${aws_s3_bucket.bucket_production.*.bucket[count.index]}"

        forwarded_values {
            query_string = false

            cookies {
                forward = "none"
            }
        }

        viewer_protocol_policy = "redirect-to-https"
        min_ttl                = 0
        default_ttl            = 60
        max_ttl                = 300
    }

    price_class = "PriceClass_All"

    restrictions {
        geo_restriction {
            restriction_type = "none"
        }
    }

    tags = {
        gitlab = gitlab_project.homepage.web_url
    }

    viewer_certificate {
        acm_certificate_arn = data.aws_acm_certificate.issued.arn
        ssl_support_method = "sni-only"
    }

    custom_error_response {
        error_caching_min_ttl = 10
        error_code            = 404
        response_code         = 404
        response_page_path    = "/404.html"
    }
}

resource "aws_route53_record" "route53_record_production" {
    count = local.number_of_domains_production
    zone_id = count.index <= 1 ? data.aws_route53_zone.selected.zone_id : data.aws_route53_zone.selected_2.zone_id 
    name    = aws_s3_bucket.bucket_production.*.bucket[count.index]
    type    = "A"

    alias {
        name                   = aws_cloudfront_distribution.s3_distribution_production.*.domain_name[count.index]
        zone_id                = aws_cloudfront_distribution.s3_distribution_production.*.hosted_zone_id[count.index]
        evaluate_target_health = true
    }
}