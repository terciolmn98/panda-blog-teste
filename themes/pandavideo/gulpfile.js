const gulp = require('gulp');
const minify = require('gulp-minify');
const htmlmin = require('gulp-htmlmin');

var filesToMove = [
  'src/libs/**',
  'src/images/**',
];

gulp.task('move', function(done){
gulp.src(filesToMove, { base: './src' })
.pipe(gulp.dest('dist'));
done()
});

gulp.task('scripts', function(done) {
  gulp.src('src/js/*.js')
    .pipe(minify())
    .pipe(gulp.dest('dist/js'))
    done()
});

gulp.task('minifyhtml', () => {
  return gulp.src('src/*.html')
    .pipe(htmlmin(
      { collapseWhitespace: true }
    ))
    .pipe(htmlmin(
      {
        removeComments: true
      }
    ))
    .pipe(gulp.dest('dist'));
});

gulp.task('minifyhtmlprivacy', () => {
  return gulp.src('src/privacy/*.html')
    .pipe(htmlmin(
      { collapseWhitespace: true }
    ))
    .pipe(htmlmin(
      {
        removeComments: true
      }
    ))
    .pipe(gulp.dest('dist/privacy'));
});

gulp.task('minifyhtmlterms', () => {
  return gulp.src('src/terms/*.html')
    .pipe(htmlmin(
      { collapseWhitespace: true }
    ))
    .pipe(htmlmin(
      {
        removeComments: true
      }
    ))
    .pipe(gulp.dest('dist/terms'));
});

gulp.task('minifyhtmlcookies', () => {
  return gulp.src('src/cookies/*.html')
    .pipe(htmlmin(
      { collapseWhitespace: true }
    ))
    .pipe(htmlmin(
      {
        removeComments: true
      }
    ))
    .pipe(gulp.dest('dist/cookies'));
});

gulp.task('default', gulp.series(['scripts', 'move', 'minifyhtml', 'minifyhtmlprivacy', 'minifyhtmlcookies', 'minifyhtmlterms']));
